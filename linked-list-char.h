/**
 *
 * Linked-list header 
 * 
 * Include this file in order to be able to call the 
 * functions available in linked-list.c. We include
 * here only those information we want to make visible
 * to other files.
 *
 * Lluis Garrido, 2014.
 *
 */
#include "include/includes.h"
#define TYPE_LIST_CHAR_PRIMARY_KEY UINT16 *
/**
 * 
 * The TYPE_LIST_PRIMARY_KEY is used to define the type of the primary
 * primary_key used to index data in the list. By default it is an integer
 * (int). 
 *
 */

/**
 *
 * This structure holds the information to be stored at each list item.  Change
 * this structure according to your needs.  In order to make this library work,
 * you also need to adapt the functions compEQ and freeListData. For the
 * current implementation the "key" member is used search within the list. 
 *
 */

typedef struct ListDataChar_ {
  // The variable used to index the list has to be called "primary_key".
  TYPE_LIST_CHAR_PRIMARY_KEY primary_key;

  // This is the additional information that will be stored
  // within the structure. This additional information is associated
  // to the primary_key.

  int counter;
} ListDataChar;


/**
 * 
 * The item structure
 *
 */

typedef struct ListItemChar_ {
  ListDataChar *data;
  struct ListItemChar_ *next;
} ListItemChar;

/**
 * 
 * The list structure
 *
 */

typedef struct ListChar_ {
  int numItems;
  ListItemChar *first;
} ListChar;

/**
 *
 * Function heders we want to make visible so that they
 * can be called from any other file.
 *
 */

void initListChar(ListChar *l);
void insertListChar(ListChar *l, TYPE_LIST_CHAR_PRIMARY_KEY data);
ListDataChar *findListChar(ListChar *l, TYPE_LIST_CHAR_PRIMARY_KEY primary_key);
void deleteFirstListChar(ListChar *l);
void deleteListChar(ListChar *l);
void dumpListChar(ListChar *l);

