#include "include/includes.h"
#include "include/blink.h"
#include "include/basic_rf/basic_rf.h"
#include "include/hal/msp430/hal.h"
#include "include/hal/msp430/hal_msp430FET.h"
#include "include/hal/hal_cc2420.h"
#include "linked-list-char.h"

BOOL bucleInicial = TRUE;
// The RF settings structure is declared here, since we'll always need halRfInit()
volatile BASIC_RF_SETTINGS rfSettings;

//-----------------------------------------------------------------------------------
// PRIVATE CONSTANTS


UINT8 RegValue;

#define PAYLOAD_SIZE	1
#define RF_CHANNEL		15
#define TX_PERIOD       50  // Packet sent each n'th cycle

#define PANID			0x2420
#define MYADDR			0xCAFE
#define DESADDR			0x029A
#define BROADCAST		0xFFFF//0xFFFF //

#ifdef __ICC430__
#define FILL_UINT8    0xFF
#else
#define FILL_UINT8    0xEE
#endif
#define ACK	0x11
#define NACK 	0x10
#define SLEEP	0x00
#define INFO	0x01
//-----------------------------------------------------------------------------------
// PRIVATE DATA

static UINT8 ledPeriod= 0x80;    // LED blinking frequency
static UINT16 nRecv =  0;       // Counting received packets
ListChar *direccions;
// Basic RF transmission and reception structures
static BASIC_RF_RX_INFO rfRxInfo;
static BASIC_RF_TX_INFO rfTxInfo;
static UINT8 pTxBuffer[BASIC_RF_MAX_PAYLOAD_SIZE];
static UINT8 pRxBuffer[BASIC_RF_MAX_PAYLOAD_SIZE];
int ackReceived;
typedef enum{
	RECIVING,
	FREE,
	WAITING,
	SENDING,
	SLEEPING,
	INFORMATING
} stats;

int counterSender=0;
UINT8 packageRecived=FREE;
UINT8 broadcasting=SENDING;
UINT8 sendState= FREE;
UINT8 sendToAll=FREE;
//-----------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES

//static void ledFlash(UINT16 duration, UINT16 period);

int recivePackage(void){

	UINT16 frameControlField;
	INT8 length;
	UINT8 pFooter[2];

    CLEAR_FIFOP_INT();


    // Clean up and exit in case of FIFO overflow, which is indicated by FIFOP = 1 and FIFO = 0
	if((FIFOP_IS_1) && (!(FIFO_IS_1))) {
	    FASTSPI_STROBE(CC2420_SFLUSHRX);
	    FASTSPI_STROBE(CC2420_SFLUSHRX);
	    return 1;
	}

	// Payload length
	FASTSPI_READ_FIFO_UINT8(length);

	length &= BASIC_RF_LENGTH_MASK; // Ignore MSB

    // Ignore the packet if the length is too short
    if (length < BASIC_RF_ACK_PACKET_SIZE) {
    	FASTSPI_READ_FIFO_GARBAGE(length);

        // Otherwise, if the length is valid, then proceed with the rest of the packet
    } else {
        // Register the payload length
        rfSettings.pRxInfo->length = length - BASIC_RF_PACKET_OVERHEAD_SIZE;

        // Read the frame control field and the data sequence number
        FASTSPI_READ_FIFO_NO_WAIT((UINT8*) &frameControlField, 2);
        rfSettings.pRxInfo->ackRequest = !!(frameControlField & BASIC_RF_FCF_ACK_BM);
    	FASTSPI_READ_FIFO_UINT8(rfSettings.pRxInfo->seqNumber);

		// Is this an acknowledgment packet?
    	if ((length == BASIC_RF_ACK_PACKET_SIZE) && (frameControlField == BASIC_RF_ACK_FCF)
            && (rfSettings.pRxInfo->seqNumber == rfSettings.txSeqNumber)) {

 	       	// Read the footer and check for CRC OK
			FASTSPI_READ_FIFO_NO_WAIT((UINT8*) pFooter, 2);

			// Indicate the successful ack reception (this flag is pol by the transmission routine) //&&rfSettings.pRxInfo->srcAddr==rfTxInfo.destAddr
			if ((pFooter[1] & BASIC_RF_CRC_OK_BM)) rfSettings.ackReceived = TRUE;

            // Too small to be a valid packet?
		} else if (length < BASIC_RF_PACKET_OVERHEAD_SIZE) {
			FASTSPI_READ_FIFO_GARBAGE(length - 3);
			return 1;

            // Receive the rest of the packet
		} else {

			// Skip the destination PAN and address (that's taken care of by harware address recognition!)
			FASTSPI_READ_FIFO_GARBAGE(2);

			// Read the source address
			FASTSPI_READ_FIFO_NO_WAIT((UINT8*) &rfSettings.pRxInfo->destAddr, 2);


			// Read the source address
			FASTSPI_READ_FIFO_NO_WAIT((UINT8*) &rfSettings.pRxInfo->srcAddr, 2);
			if (rfSettings.pRxInfo->destAddr==MYADDR){
			    insertListChar(direccions,&rfSettings.pRxInfo->srcAddr);
			}

			// Read the packet payload
			FASTSPI_READ_FIFO_NO_WAIT(rfSettings.pRxInfo->pPayload, rfSettings.pRxInfo->length);
			// Read the footer to get the RSSI value
			FASTSPI_READ_FIFO_NO_WAIT((UINT8*) pFooter, 2);
			rfSettings.pRxInfo->rssi = pFooter[0];

			// Notify the application about the received _data_ packet if the CRC is OK
			//if (((frameControlField & (BASIC_RF_FCF_BM)) == BASIC_RF_FCF_NOACK) && (pFooter[1] & BASIC_RF_CRC_OK_BM)) {
			//	rfSettings.pRxInfo= basicRfReceivePacket(rfSettings.pRxInfo);
			//}
		}
    }
    CLEAR_FIFOP_INT();
    ENABLE_FIFOP_INT();
    return 0;
}
int sendPackageTo(UINT16 address,UINT8 payload){



	UINT8 status;


	rfTxInfo.destAddr = address;
	rfTxInfo.destPanId = PANID;


	// Initalize common protocol parameters
	rfTxInfo.length = PAYLOAD_SIZE;
	rfTxInfo.ackRequest = TRUE;
	rfTxInfo.pPayload = pTxBuffer;

    FASTSPI_UPD_STATUS(status);

	pTxBuffer[0]= payload;


	return basicRfSendPacket(&rfTxInfo);
}
int sendBroadcast(void){

	UINT8 status;



	rfTxInfo.destAddr = BROADCAST;
	rfTxInfo.destPanId = PANID;


    // Initalize common protocol parameters
    rfTxInfo.length = PAYLOAD_SIZE;
    rfTxInfo.ackRequest = FALSE;
    rfTxInfo.pPayload = pTxBuffer;
    rfRxInfo.pPayload = pRxBuffer;


	FASTSPI_UPD_STATUS(status);

	// Transmit information on number of packets received
	// and CC2420 status

	pTxBuffer[0]= 0xFF;



	return basicRfSendPacket(&rfTxInfo);
}
/**
void sendToAll(UINT8 payload){
  ListItemChar* item;
  item=direccions->first;
  while (item!=NULL){
    
	UINT8 status;


	rfTxInfo.destAddr = *item->data->primary_key;
	rfTxInfo.destPanId = PANID;


	// Initalize common protocol parameters
	rfTxInfo.length = PAYLOAD_SIZE;
	rfTxInfo.ackRequest = TRUE;
	rfTxInfo.pPayload = pTxBuffer;
    rfSettings.txSeqNumber++;

    FASTSPI_UPD_STATUS(status);

	pTxBuffer[0]=  (UINT8)0x29A;
    pTxBuffer[1]= status;
    pTxBuffer[2]= payload;


	// Transmit information on number of packets received
	// and CC2420 status



  }
}*/
// rfSetChannel
/*********************************************************************
 * Metode main (void)
 * description:
 * 	Startup routine and main loop
 ********************************************************************/
   int main(void) {

    UINT8 n;
    direccions=malloc(sizeof(ListChar));
    initListChar(direccions);
    ackReceived=0;
    DISABLE_FIFOP_INT();
    // Initalize ports for communication with CC2420 and other peripheral units
    PORT_INIT();
    SPI_INIT();
    InitP2_7();

    // Wait for the user to select node address, and initialize for basic RF operation
	halWait(1000);

	basicRfInit(&rfRxInfo, RF_CHANNEL, PANID, MYADDR);
	rfTxInfo.destAddr = DESADDR;
	rfTxInfo.destPanId = PANID;


    // Initalize common protocol parameters
    rfTxInfo.length = PAYLOAD_SIZE;
    rfTxInfo.ackRequest = TRUE;
    rfTxInfo.pPayload = pTxBuffer;
    rfRxInfo.pPayload = pRxBuffer;


    for (n = 0; n < PAYLOAD_SIZE; n++) {
        pTxBuffer[n] = FILL_UINT8;
    }

    // Turn on RX mode
    basicRfReceiveOn();
	SET_BLED();
	SET_RLED();
	SET_YLED();





    ENABLE_FIFOP_INT();
	// The main loop:
	ListItemChar* item;
	while (TRUE) {

		if(packageRecived==RECIVING)
		{
			recivePackage();
		    packageRecived=FREE;
		}

		if(broadcasting==SENDING){
			CLR_RLED();
			sendBroadcast();
			SET_RLED();            //INTERUPPT ACTIVE
			broadcasting=WAITING;
			if(counterSender>10){
				counterSender=0;
				broadcasting=FREE;
				sendState=SLEEPING;
				sendToAll=SENDING;
				item=direccions->first;
			}
		}
		if (sendState==SLEEPING&&sendToAll==SENDING){
			CLR_YLED();
			if(sendPackageTo(*item->data->primary_key,SLEEP)||counterSender>5){
				counterSender=0;
				if(item->next!=NULL){
					item=item->next;
				}else{
					item=direccions->first;
					sendState=INFORMATING;
				}
			}
			SET_YLED();            //INTERUPPT ACTIVE
			sendToAll=WAITING;
		}
		if (sendState==INFORMATING&&sendToAll==SENDING){
			CLR_BLED();
			if(sendPackageTo(*item->data->primary_key,INFO)||counterSender>5){
				if(item->next!=NULL){
					item=item->next;
				}else{
					item=direccions->first;
				}
			}
			SET_BLED();            //INTERUPPT ACTIVE
			sendToAll=WAITING;
		}
    }

 // main
}


/*******************************************************************************************************
 * Peripheral functions of the main...
 *******************************************************************************************************/
void InitP2_7(void){
	P2DIR &= 0X7F;
	P2SEL &= BIT7;
	P2IES |= 0x80;
	P2IE |= BIT7;
	_EINT();
}
/****************************************************************************************************
 * fUNCTION BASIC_RF_INIT
 * //-----------------------------------------------------------------------------------
//  void basicRfInit(BASIC_RF_RX_INFO *pRRI, UINT8 channel, UINT16 panId, UINT16 myAddr)
//
//  DESCRIPTION:
//      Initializes CC2420 for radio communication via the basic RF library functions. Turns on the
//		voltage regulator, resets the CC2420, turns on the crystal oscillator, writes all necessary
//		registers and protocol addresses (for automatic address recognition). Note that the crystal
//		oscillator will remain on (forever).
//
//  ARGUMENTS:
//      BASIC_RF_RX_INFO *pRRI
//          A pointer the BASIC_RF_RX_INFO data structure to be used during the first packet reception.
//			The structure can be switched upon packet reception.
//      UINT8 channel
//          The RF channel to be used (11 = 2405 MHz to 26 = 2480 MHz)
//      UINT16 panId
//          The personal area network identification number
//      UINT16 myAddr
//          The 16-bit short address which is used by this node. Must together with the PAN ID form a
//			unique 32-bit identifier to avoid addressing conflicts. Normally, in a 802.15.4 network, the
//			short address will be given to associated nodes by the PAN coordinator.
//-----------------------------------------------------------------------------------
 *******************************************************************************************************/
void basicRfInit(BASIC_RF_RX_INFO *pRRI, UINT8 channel, UINT16 panId, UINT16 myAddr){
    UINT8 n;

    //Make sure that the voltage regulator is on, and that the reset pin is inactive
    SET_VREG_ACTIVE();
    halWait(1000);
    SET_RESET_ACTIVE();
    halWait(1000);
    SET_RESET_INACTIVE();
    halWait(500);
    //DISABLE_GLOBAL_INT();
    // Register modifications
    FASTSPI_STROBE(CC2420_SXOSCON);
    FASTSPI_SETREG(CC2420_MDMCTRL0, 0x0AF2); // Turn on automatic packet acknowledgment
    FASTSPI_SETREG(CC2420_MDMCTRL1, 0x0500); // Set the correlation threshold = 20
    FASTSPI_SETREG(CC2420_IOCFG0, 0x007F);   // Set the FIFOP threshold to maximum
    FASTSPI_SETREG(CC2420_SECCTRL0, 0x01C4); // Turn off "Security enable"

    // Set the RF channel
    halRfSetChannel(channel);

    // Turn interrupts back on
	//ENABLE_GLOBAL_INT();

	// Set the protocol configuration
	rfSettings.pRxInfo = pRRI;
	rfSettings.panId = panId;
	rfSettings.myAddr = myAddr;
	rfSettings.txSeqNumber = 0;
    rfSettings.receiveOn = FALSE;
    FASTSPI_STROBE(CC2420_STXCAL);
	// Wait for the crystal oscillator to become stable
    halRfWaitForCrystalOscillator();

	// Write the short address and the PAN ID to the CC2420 RAM (requires that the XOSC is on and stable)
   	DISABLE_GLOBAL_INT();
    FASTSPI_WRITE_RAM_LE(&myAddr, CC2420RAM_SHORTADDR, 2, n);
    FASTSPI_WRITE_RAM_LE(&panId, CC2420RAM_PANID, 2, n);
    _EINT();
}

//-----------------------------------------------------------------------------------
//  UINT8 basicRfSendPacket(BASIC_RF_TX_INFO *pRTI)
//
//  DESCRIPTION:
//		Transmits a packet using the IEEE 802.15.4 MAC data packet format with short addresses. CCA is
//		measured only once before backet transmission (not compliant with 802.15.4 CSMA-CA).
//		The function returns:
//			- When pRTI->ackRequest is FALSE: After the transmission has begun (SFD gone high)
//			- When pRTI->ackRequest is TRUE: After the acknowledgment has been received/declared missing.
//		The acknowledgment is received through the FIFOP interrupt.
//
//  ARGUMENTS:
//      BASIC_RF_TX_INFO *pRTI
//          The transmission structure, which contains all relevant info about the packet.
//
//  RETURN VALUE:
//		BOOL
//			Successful transmission (acknowledgment received)
//-----------------------------------------------------------------------------------


BOOL basicRfSendPacket(BASIC_RF_TX_INFO *pRTI)
{
	UINT16 frameControlField;
    UINT8 packetLength;
    BOOL success;
    UINT8 spiStatusUINT8;

    // Wait until the transceiver is idle
    while (FIFOP_IS_1 || SFD_IS_1);

    // Turn off global interrupts to avoid interference on the SPI interface
    DISABLE_GLOBAL_INT();

	// Flush the TX FIFO just in case...
	FASTSPI_STROBE(CC2420_SFLUSHTX);

    // Turn on RX if necessary
    if (!rfSettings.receiveOn) FASTSPI_STROBE(CC2420_SRXON);

    // Wait for the RSSI value to become valid
    do {
        FASTSPI_UPD_STATUS(spiStatusUINT8);
    } while (!(spiStatusUINT8 & BM(CC2420_RSSI_VALID)));

    packetLength = pRTI->length + BASIC_RF_PACKET_OVERHEAD_SIZE;
    frameControlField = pRTI->ackRequest ? BASIC_RF_FCF_ACK : BASIC_RF_FCF_NOACK;


    FASTSPI_WRITE_FIFO((UINT8*)&packetLength, 1);             // Packet length
    FASTSPI_WRITE_FIFO((UINT8*)&frameControlField, 2);         // Frame control field
    FASTSPI_WRITE_FIFO((UINT8*)&rfSettings.txSeqNumber, 1);    // Sequence number
    FASTSPI_WRITE_FIFO((UINT8*)&rfSettings.panId, 2);          // Dest. PAN ID
    FASTSPI_WRITE_FIFO((UINT8*)&pRTI->destAddr, 2);            // Dest. address
    FASTSPI_WRITE_FIFO((UINT8*)&rfSettings.myAddr, 2);         // Source address
	FASTSPI_WRITE_FIFO((UINT8*)pRTI->pPayload, pRTI->length);  // Payload

	// Wait for the transmission to begin before exiting (makes sure that this
    // function cannot be calld a second time, and thereby cancelling the first
    // transmission (observe the FIFOP + SFD test above).

	FASTSPI_STROBE(CC2420_STXONCCA);
	FASTSPI_GETREG(CC2420_FSMSTATE,RegValue);

	// Turn interrupts back on
	ENABLE_GLOBAL_INT();
	//while (!SFD_IS_1); //Probamos comentando esto para ver si funciona.

    // Wait for the acknowledge to be received, if any
    if (pRTI->ackRequest) {
		rfSettings.ackReceived = FALSE;

		// Wait for the SFD to go low again
		while (SFD_IS_1);

        // We'll enter RX automatically, so just wait until we can be sure that the
        // ack reception should have finished. The timeout consists of a 12-symbol
        // turnaround time, the ack packet duration, and a small margin
        halWait((12 * BASIC_RF_SYMBOL_DURATION) + (BASIC_RF_ACK_DURATION) +
                (2 * BASIC_RF_SYMBOL_DURATION) + 100);

		// If an acknowledgment has been received (by the FIFOP interrupt),
        // the ackReceived flag should be set
		success = rfSettings.ackReceived;
    } else {
        success= TRUE;
    }

	// Turn off the receiver if it should not continue to be enabled
    DISABLE_GLOBAL_INT();
	if (!rfSettings.receiveOn) FASTSPI_STROBE(CC2420_SRFOFF);
    ENABLE_GLOBAL_INT();

    // Increment the sequence number, and return the result
    rfSettings.txSeqNumber++;
    return success;

} // halRfSendPacket


//-----------------------------------------------------------------------------------
//  void halRfReceiveOn(void)
//
//  DESCRIPTION:
//      Enables the CC2420 receiver and the FIFOP interrupt. When a packet is received through this
//      interrupt, it will call halRfReceivePacket(...), which must be defined by the application
//-----------------------------------------------------------------------------------
void basicRfReceiveOn(void)
{
    rfSettings.receiveOn = TRUE;
	FASTSPI_STROBE(CC2420_SRXON);
	FASTSPI_STROBE(CC2420_SFLUSHRX);
	FIFOP_INT_INIT();
    ENABLE_FIFOP_INT();
} // basicRfReceiveOn




//-----------------------------------------------------------------------------------
//  void halRfReceiveOff(void)
//
//  DESCRIPTION:
//      Disables the CC2420 receiver and the FIFOP interrupt.
//-----------------------------------------------------------------------------------
void basicRfReceiveOff(void)
{
    rfSettings.receiveOn = FALSE;
	FASTSPI_STROBE(CC2420_SRFOFF);
    DISABLE_FIFOP_INT();
} // basicRfReceiveOff


/* Deep Sleep
void rfshutdown(void)
{
SET_RESET_ACTIVE();
SET_VREG_INACTIVE();
SPI_DISABLE();
}
*/





//***********************************************************************************************************
//***********************************************************************************************************
//interrupt handler
// PORT 1
// PORT 2
//***********************************************************************************************************
//***********************************************************************************************************
#pragma vector=PORT2_VECTOR
__interrupt void P2_ISR(void){
	
	if((P2IFG && 0x80)==1){
		//__low_power_mode_4();
		halWait(100);
		//__low_power_mode_off_on_exit();  // MSP430 SLEEP
		P2IFG &= 0x7F;//clear flag from user button
		broadcasting=SENDING;
	}
	//__low_power_mode_off_on_exit()
	
}
//-----------------------------------------------------------------------------------
//  SIGNAL(SIG_INTERRUPT0) - CC2420 FIFOP interrupt service routine
//
//  DESCRIPTION:
//		When a packet has been completely received, this ISR will extract the data from the RX FIFO, put
//		it into the active BASIC_RF_RX_INFO structure, and call basicRfReceivePacket() (defined by the
//		application). FIFO overflow and illegally formatted packets is handled by this routine.
//
//      Note: Packets are acknowledged automatically by CC2420 through the auto-acknowledgment feature.
//-----------------------------------------------------------------------------------

#pragma vector=PORT1_VECTOR
__interrupt void fifo_rx(void){
	packageRecived=RECIVING;
	DISABLE_FIFOP_INT();
} // SIGNAL(SIG_INTERRUPT0)

#pragma vector=WDT_VECTOR
__interrupt void WDT_Interval(void){
P1IFG &= ~WDTIFG;
if (broadcasting==WAITING){
	broadcasting=SENDING;
}
counterSender++;
if(sendToAll==WAITING){
	sendToAll=SENDING;
}
}

