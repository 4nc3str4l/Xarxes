/**
 *
 * Linked-list implementation. 
 * 
 * This is an implementation of a simple linked-list. A minimal
 * set of necessary functions have been included.
 *
 * Lluis Garrido, 2014.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * We include the linked-list.h header. Note the double
 * quotes.
 */

#include "linked-list-char.h"

/**
 *
 * Free data element. The user should adapt this function to their needs.  This
 * function is called internally by deleteList.
 *
 */

static void freeListDataChar(ListDataChar *data)
{
  free(data); 
}

/**
 *
 * Dumps data contents to stdout. To be used for debugging.
 *
 */

static void dumpListDataChar(ListDataChar *data)
{
  printf("Key %03d appears %d times\n", data->primary_key, data->counter);
}

/**
 *
 * Compares if primary_key1 is equal to primary_key2. Should return 1 (true) if condition
 * is satisfied, 0 (false) otherwise.
 *
 */

static int compEQChar(TYPE_LIST_CHAR_PRIMARY_KEY primary_key1, TYPE_LIST_CHAR_PRIMARY_KEY primary_key2)
{
  return primary_key1==primary_key2;
}

/**
 * 
 * Please do not change the code below unless you really know what you are
 * doing.
 *
 */

/**
 * 
 * Initialize an empty list
 *
 */

void initListChar(ListChar *l)
{
  l->numItems = 0;
  l->first = NULL;
}

/**
 * 
 * Insert data in the list.  This function does not perform a copy of data
 * when inserting it in the list, it rather creates a list item and makes
 * this item point to the data. Thus, the contents of data should not be
 * overwritten after calling this function.
 *
 */

void insertListChar(ListChar *l,TYPE_LIST_CHAR_PRIMARY_KEY cadena)
{
  ListDataChar *data;
  data=findListChar(l,cadena);
  if (data!=NULL){
    data->counter++;
  }else{
    data=malloc(sizeof(ListDataChar));
    ListItemChar *tmp, *x;
    x = malloc(sizeof(ListItemChar));

    if (x == 0) {
      printf("insufficient memory (insertItem)\n");
      exit(1);
    }

    /* Insert item at first position */

    tmp = l->first;
    l->first = x;
    x->next = tmp;
    x->data=data;
    x->data->primary_key=malloc(100*sizeof(char));
    /* Link data to inserted item */
    x->data->primary_key=cadena;
    //x->data->primary_key=cadena;
    x->data->counter=1;

    l->numItems++;
  }
    
  }

/**
 * 
 * Find item containing the specified primary_key. Returns the data
 * that it points to (not the item itself).
 *
 */

ListDataChar *findListChar(ListChar *l, TYPE_LIST_CHAR_PRIMARY_KEY primary_key)
{
  ListItemChar *current;

  current = l->first;

  while (current != NULL)
  {
    if (compEQChar(current->data->primary_key, primary_key)){
      return (current->data);
    }

    current = current->next;
  }

  return (NULL);
}

/**
 * 
 * Deletes the first item of the list. The data to which
 * the deleted item points to also is deleted.
 *
 */

void deleteFirstListChar(ListChar *l)
{
  ListItemChar *tmp;

  tmp = l->first;

  if (tmp)
  {
    l->first = tmp->next;
    freeListDataChar(tmp->data);
    free(tmp);
    l->numItems--;
  }
}

/**
 * 
 * Deletes a list including the data to which their 
 * items point to.
 *
 */

void deleteListChar(ListChar *l)
{
  ListItemChar *current, *next;

  current = l->first;

  while (current != NULL)
  {
    next = current->next;
    freeListDataChar(current->data);
    free(current);
    current = next;
  }

  l->numItems = 0;
  l->first = NULL;
}

/**
 * 
 * Dumps the contents of the list. Internally this function
 * called dumpListData which is user defined.
 *
 */

void dumpListChar(ListChar *l)
{
  ListItemChar *current;

  current = l->first;

  while (current != NULL)
  {
    dumpListDataChar(current->data);
    current = current->next;
  }

  printf("Total number of items: %d\n", l->numItems);
}

